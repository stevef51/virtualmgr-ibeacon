var exec = require('cordova/exec');

var _module = "VirtualMgr_iBeacon";

function Client(id, options) {
	this.id = id;
	this.options = options || {
		deleteExistingClient: false
	};
	if (this.options.deleteExistingClient) {
		exec(null, null, _module, "deleteClient", [this.id]);
	}
}

Client.prototype.actionCallback = function(success, error) {
	var self = this;
	exec(success, error, _module, "clientActionCallback", [self.id]);
}

Client.prototype.requestAlwaysAuthorisation = function(success, error) {
	var self = this;
	exec(success, error, _module, "clientRequestAlwaysAuthorisation", [self.id]);
}

Client.prototype.registerRegion = function(region, success, error) {
	var self = this;
	exec(success, error, _module, "clientRegisterRegion", [self.id, region]);
}

Client.prototype.startRangingBeaconsInRegion = function (regionIdentifier, success, error) {
	var self = this;
	exec(success, error, _module, "clientStartRangingBeaconsInRegion", [self.id, regionIdentifier]);
}

Client.prototype.stopRangingBeaconsInRegion = function (regionIdentifier, success, error) {
	var self = this;
	exec(success, error, _module, "clientStopRangingBeaconsInRegion", [self.id, regionIdentifier]);
}

Client.prototype.startMonitoringForRegion = function (regionIdentifier, success, error) {
	var self = this;
	exec(success, error, _module, "clientStartMonitoringForRegion", [self.id, regionIdentifier]);
}

Client.prototype.stopMonitoringForRegion = function (regionIdentifier, success, error) {
	var self = this;
	exec(success, error, _module, "clientStopMonitoringForRegion", [self.id, regionIdentifier]);
}

Client.prototype.stopRangingForAllBeacons = function (success, error) {
	var self = this;
	exec(success, error, _module, "clientStopRangingForAllBeacons", [self.id]);
}

Client.prototype.requestStateForRegion = function (regionIdentifier, success, error) {
	var self = this;
	exec(success, error, _module, "clientRequestStateForRegion", [self.id, regionIdentifier]);
}

exports.Client = Client;
