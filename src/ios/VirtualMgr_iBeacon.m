/********* VirtualMgr_iBeacon.m Cordova Plugin Implementation *******/

#import <Cordova/CDV.h>
#import <CoreLocation/CoreLocation.h>

#define DEBUGLOG(fmt, ...) NSLog(fmt, ##__VA_ARGS__)
//#define DEBUGLOG(x, ...)

#define PLUGIN_VERSION @"1.0.0"

NSMutableDictionary* getBeaconInfo(CLBeacon* beacon)
{
    NSMutableDictionary* info = [[NSMutableDictionary alloc] init];
    [info setObject:beacon.major forKey:@"major"];
    [info setObject:beacon.minor forKey:@"minor"];
    [info setObject:[NSNumber numberWithInt: beacon.proximity] forKey:@"proximity"];
    [info setObject:[beacon.proximityUUID description] forKey:@"proximityUUID"];
    return info;
}

NSMutableDictionary* getBeaconRegionInfo(CLBeaconRegion* region)
{
    NSMutableDictionary* info = [[NSMutableDictionary alloc] init];
    if (region.major != nil) {
        [info setObject:region.major forKey:@"major"];
    }
    if (region.minor != nil) {
        [info setObject:region.minor forKey:@"minor"];
    }
    [info setObject:[region.proximityUUID description] forKey:@"proximityUUID"];
    [info setObject:region.identifier forKey:@"identifier"];
    return info;
}

NSString* getRegionState(CLRegionState state)
{
    switch(state) {
        case CLRegionStateInside:
            return @"Inside";

        case CLRegionStateOutside:
            return @"Outside";

        case CLRegionStateUnknown:
            return @"Unknown";

    }
    return @"Invalid State";
}

@interface iBeaconScanClient : NSObject<CLLocationManagerDelegate> {

}

@property (nonatomic, retain) NSString* clientId;
@property (nonatomic, retain) CLLocationManager* locationManager;
@property (nonatomic, retain) id<CDVCommandDelegate> commandDelegate;
@property (nonatomic, retain) NSString* actionCallbackId;
@property (nonatomic, retain) NSString* requestAuthorisationCallbackId;
@property (nonatomic, retain) NSMutableDictionary* regions;

@end

@implementation iBeaconScanClient

@synthesize clientId, locationManager, commandDelegate, actionCallbackId, requestAuthorisationCallbackId, regions;

const int firstParameterOffset = 1;

-(id)initClientId:(NSString*) theClientId withCommandDelegate:(id<CDVCommandDelegate>)theCommandDelegate
{
    if ((self = [super init])) {
        DEBUGLOG(@"iBeaconScanClient initClientId: %@", theClientId);
        self.clientId = theClientId;
        self.commandDelegate = theCommandDelegate;
        self.locationManager = [[CLLocationManager alloc] init];

        if ([locationManager respondsToSelector:@selector(requestAlwaysAuthorization)]) {
            [locationManager requestAlwaysAuthorization];
        }
        self.locationManager.delegate = self;
        self.regions = [[NSMutableDictionary alloc] init];
    }
    return self;
}

-(void)dispose
{
    DEBUGLOG(@"iBeaconScanClient dispose: %@", self.clientId);
    self.clientId = nil;
    self.commandDelegate = nil;
    self.locationManager = nil;
    self.actionCallbackId = nil;
    self.requestAuthorisationCallbackId = nil;
    self.regions = nil;
}

-(CLBeaconRegion*)parseBeaconRegion:(NSDictionary*)args withPluginResult:(CDVPluginResult**)outPluginResult
{
    NSUUID* proximityUUID = nil;
    NSString* proximityUUIDString = nil;
    NSNumber* major = nil;
    NSNumber* minor = nil;
    NSString* identifier = nil;

    proximityUUIDString = [args objectForKey:@"proximityUUID"];
    if (proximityUUIDString == nil) {
        *outPluginResult = [CDVPluginResult resultWithStatus: CDVCommandStatus_ERROR messageAsString: @"Missing argument 'proximityUUID'"];
    } else {
        proximityUUID = [[NSUUID alloc] initWithUUIDString:proximityUUIDString];
        if (proximityUUID == nil) {
            *outPluginResult = [CDVPluginResult resultWithStatus: CDVCommandStatus_ERROR messageAsString: @"Invalid UUID 'proximityUUID'"];
        }

        major = [args objectForKey:@"major"];
        minor = [args objectForKey:@"minor"];
        identifier = [args objectForKey:@"identifier"];
        if (identifier == nil) {
            *outPluginResult = [CDVPluginResult resultWithStatus: CDVCommandStatus_ERROR messageAsString: @"Missing argument 'identifier'"];
        }
    }

    CLBeaconRegion* region = nil;
    if (*outPluginResult == nil) {
        if (major != nil) {
            if (minor != nil) {
                region = [[CLBeaconRegion alloc] initWithProximityUUID:proximityUUID major:[major integerValue] minor:[minor integerValue] identifier:identifier];
            } else {
                region = [[CLBeaconRegion alloc] initWithProximityUUID:proximityUUID major:[major integerValue] identifier:identifier];
            }
        } else {
            region = [[CLBeaconRegion alloc] initWithProximityUUID:proximityUUID identifier:identifier];
        }
    }
    return region;
}

-(void)actionCallback:(CDVInvokedUrlCommand*)command
{
    self.actionCallbackId = command.callbackId;
    CDVPluginResult* pluginResult = [CDVPluginResult resultWithStatus: CDVCommandStatus_OK messageAsArray: nil];
    [pluginResult setKeepCallbackAsBool:TRUE];
    [self.commandDelegate sendPluginResult: pluginResult callbackId: actionCallbackId];
}

-(void)locationManager:(CLLocationManager *)manager didChangeAuthorizationStatus:(CLAuthorizationStatus)status
{
    CDVPluginResult* pluginResult = nil;
    if (requestAuthorisationCallbackId != nil) {
        pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsInt:status];
        [self.commandDelegate sendPluginResult: pluginResult callbackId: requestAuthorisationCallbackId];
        self.requestAuthorisationCallbackId = nil;
    }
}

-(void)requestAlwaysAuthorisation:(CDVInvokedUrlCommand*) command
{
    CDVPluginResult* pluginResult = nil;

    if ([CLLocationManager authorizationStatus] == kCLAuthorizationStatusNotDetermined) {
        if ([locationManager respondsToSelector:@selector(requestAlwaysAuthorization)]) {
            self.requestAuthorisationCallbackId = command.callbackId;
            [locationManager requestAlwaysAuthorization];
        } else {
            pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsInt:-1];
            [self.commandDelegate sendPluginResult: pluginResult callbackId: command.callbackId];
        }
    } else {
        pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsInt:[CLLocationManager authorizationStatus]];
        [self.commandDelegate sendPluginResult: pluginResult callbackId: command.callbackId];
    }
}

-(void)registerRegion:(CDVInvokedUrlCommand*) command
{
    NSDictionary* args = nil;
    CDVPluginResult* pluginResult = nil;

    if (command.arguments.count >= firstParameterOffset + 1) {
        args = [command.arguments objectAtIndex:firstParameterOffset + 0];

        CLBeaconRegion* region = [self parseBeaconRegion:args withPluginResult:&pluginResult];
        if (pluginResult == nil) {
            if ([self.regions objectForKey: region.identifier] == nil) {
                [self.regions setObject:region forKey:region.identifier];
            } else {
                pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_ERROR messageAsString:@"Duplicate region"];
            }
        }
    } else {
        pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_ERROR messageAsString:@"Missing 1st argument"];
    }

    [self.commandDelegate sendPluginResult: pluginResult callbackId: command.callbackId];
}

-(void)startMonitoringForRegion:(CDVInvokedUrlCommand*)command
{
    CDVPluginResult* pluginResult = nil;

    DEBUGLOG(@"startMonitoringForRegion");

    if (command.arguments.count >= firstParameterOffset + 1) {
        NSString* identifier = [command.arguments objectAtIndex:firstParameterOffset + 0];

        CLBeaconRegion* region = [self.regions objectForKey:identifier];
        if (region != nil) {
            [locationManager startMonitoringForRegion:region];

            pluginResult = [CDVPluginResult resultWithStatus: CDVCommandStatus_OK messageAsArray: nil];
        } else {
            pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_ERROR messageAsString:@"Region not found"];
        }
    } else {
        pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_ERROR messageAsString:@"Missing region identifier"];
    }

    [self.commandDelegate sendPluginResult: pluginResult callbackId: command.callbackId];
}

-(void)stopMonitoringForRegion:(CDVInvokedUrlCommand*)command
{
    CDVPluginResult* pluginResult = nil;

    DEBUGLOG(@"stopMonitoringForRegion");
    if (command.arguments.count >= firstParameterOffset + 1) {
        NSString* identifier = [command.arguments objectAtIndex:firstParameterOffset + 0];

        CLBeaconRegion* region = [self.regions objectForKey:identifier];
        if (region != nil) {
            [locationManager stopMonitoringForRegion:region];

            pluginResult = [CDVPluginResult resultWithStatus: CDVCommandStatus_OK messageAsArray: nil];
        } else {
            pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_ERROR messageAsString:@"Region not found"];
        }
    } else {
        pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_ERROR messageAsString:@"Missing region identifier"];
    }

    [self.commandDelegate sendPluginResult: pluginResult callbackId: command.callbackId];
}

-(void)startRangingBeaconsInRegion:(CDVInvokedUrlCommand*)command
{
    CDVPluginResult* pluginResult = nil;

    DEBUGLOG(@"startRangingBeaconsInRegion");
    if (command.arguments.count >= firstParameterOffset + 1) {
        NSString* identifier = [command.arguments objectAtIndex:firstParameterOffset + 0];

        CLBeaconRegion* region = [self.regions objectForKey:identifier];
        if (region != nil) {
            [locationManager startRangingBeaconsInRegion:region];

            pluginResult = [CDVPluginResult resultWithStatus: CDVCommandStatus_OK messageAsArray: nil];
        } else {
            pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_ERROR messageAsString:@"Region not found"];
        }
    } else {
        pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_ERROR messageAsString:@"Missing region identifier"];
    }

    [self.commandDelegate sendPluginResult: pluginResult callbackId: command.callbackId];
}

-(void)stopRangingBeaconsInRegion:(CDVInvokedUrlCommand*)command
{
    CDVPluginResult* pluginResult = nil;

    DEBUGLOG(@"stopRangingBeaconsInRegion");
    if (command.arguments.count >= firstParameterOffset + 1) {
        NSString* identifier = [command.arguments objectAtIndex:firstParameterOffset + 0];

        CLBeaconRegion* region = [self.regions objectForKey:identifier];
        if (region != nil) {
            [locationManager stopRangingBeaconsInRegion:region];

            pluginResult = [CDVPluginResult resultWithStatus: CDVCommandStatus_OK messageAsArray: nil];
        } else {
            pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_ERROR messageAsString:@"Region not found"];
        }
    } else {
        pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_ERROR messageAsString:@"Missing region identifier"];
    }

    [self.commandDelegate sendPluginResult: pluginResult callbackId: command.callbackId];
}

-(void)requestStateForRegion:(CDVInvokedUrlCommand*)command
{
    CDVPluginResult* pluginResult = nil;

    DEBUGLOG(@"requestStateForRegion");
    if (command.arguments.count >= firstParameterOffset + 1) {
        NSString* identifier = [command.arguments objectAtIndex:firstParameterOffset + 0];

        CLBeaconRegion* region = [self.regions objectForKey:identifier];
        if (region != nil) {
            [locationManager requestStateForRegion:region];

            pluginResult = [CDVPluginResult resultWithStatus: CDVCommandStatus_OK messageAsArray: nil];
        } else {
            pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_ERROR messageAsString:@"Region not found"];
        }
    } else {
        pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_ERROR messageAsString:@"Missing region identifier"];
    }

    [self.commandDelegate sendPluginResult: pluginResult callbackId: command.callbackId];
}

-(void)stopRangingForAllBeacons:(CDVInvokedUrlCommand*)command
{
    CDVPluginResult* pluginResult = nil;

    for(CLRegion* region in locationManager.monitoredRegions) {
        [locationManager stopRangingBeaconsInRegion:(CLBeaconRegion*)region];
    }

    pluginResult = [CDVPluginResult resultWithStatus: CDVCommandStatus_OK messageAsArray: nil];
    [self.commandDelegate sendPluginResult: pluginResult callbackId: command.callbackId];
}

-(void)locationManager:(CLLocationManager *)manager didRangeBeacons:(NSArray<CLBeacon *> *)beacons inRegion:(CLBeaconRegion *)region
{
    DEBUGLOG(@"didRangeBeacons");

    NSMutableDictionary* jResult = [[NSMutableDictionary alloc] init];
    NSMutableArray* jBeacons = [[NSMutableArray alloc] init];
    for(CLBeacon* beacon in beacons) {
        [jBeacons addObject:getBeaconInfo(beacon)];
    }

    [jResult setObject:@"beacons" forKey:@"action"];
    [jResult setObject:jBeacons forKey:@"beacons"];
    [jResult setObject:getBeaconRegionInfo(region) forKey:@"region"];

    CDVPluginResult* pluginResult = [CDVPluginResult resultWithStatus: CDVCommandStatus_OK messageAsDictionary:jResult];
    [pluginResult setKeepCallbackAsBool:TRUE];

    [self.commandDelegate sendPluginResult: pluginResult callbackId: actionCallbackId];
}

-(void)locationManager:(CLLocationManager *)manager didDetermineState:(CLRegionState)state forRegion:(CLRegion *)region
{
    DEBUGLOG(@"didDetermineState");
    NSMutableDictionary* jResult = [[NSMutableDictionary alloc] init];

    [jResult setObject:@"state" forKey:@"action"];
    [jResult setObject:getRegionState(state) forKey:@"state"];
    [jResult setObject:getBeaconRegionInfo((CLBeaconRegion*)region) forKey:@"region"];

    CDVPluginResult* pluginResult = [CDVPluginResult resultWithStatus: CDVCommandStatus_OK messageAsDictionary:jResult];
    [pluginResult setKeepCallbackAsBool:TRUE];

    [self.commandDelegate sendPluginResult: pluginResult callbackId: actionCallbackId];
}

-(void)locationManager:(CLLocationManager *)manager didExitRegion:(CLRegion *)region
{
    DEBUGLOG(@"didExitRegion");
    NSMutableDictionary* jResult = [[NSMutableDictionary alloc] init];

    [jResult setObject:@"exit" forKey:@"action"];
    [jResult setObject:getBeaconRegionInfo((CLBeaconRegion*)region) forKey:@"region"];

    CDVPluginResult* pluginResult = [CDVPluginResult resultWithStatus: CDVCommandStatus_OK messageAsDictionary:jResult];
    [pluginResult setKeepCallbackAsBool:TRUE];

    [self.commandDelegate sendPluginResult: pluginResult callbackId: actionCallbackId];
}

-(void)locationManager:(CLLocationManager *)manager didEnterRegion:(nonnull CLRegion *)region
{
    DEBUGLOG(@"didEnterRegion");
    NSMutableDictionary* jResult = [[NSMutableDictionary alloc] init];

    [jResult setObject:@"enter" forKey:@"action"];
    [jResult setObject:getBeaconRegionInfo((CLBeaconRegion*)region) forKey:@"region"];

    CDVPluginResult* pluginResult = [CDVPluginResult resultWithStatus: CDVCommandStatus_OK messageAsDictionary:jResult];
    [pluginResult setKeepCallbackAsBool:TRUE];

    [self.commandDelegate sendPluginResult: pluginResult callbackId: actionCallbackId];
}

-(void)locationManager:(CLLocationManager *)manager monitoringDidFailForRegion:(CLRegion *)region withError:(NSError *)error
{
    DEBUGLOG(@"monitoringDidFailForRegion");

    NSMutableDictionary* jResult = [[NSMutableDictionary alloc] init];

    [jResult setObject:@"fail" forKey:@"action"];
    [jResult setObject:getBeaconRegionInfo((CLBeaconRegion*)region) forKey:@"region"];
    [jResult setObject:[NSNumber numberWithLong:[error code]] forKey:@"code"];
    [jResult setObject:[error description] forKey:@"description"];

    CDVPluginResult* pluginResult = [CDVPluginResult resultWithStatus: CDVCommandStatus_OK messageAsDictionary:jResult];
    [pluginResult setKeepCallbackAsBool:TRUE];

    [self.commandDelegate sendPluginResult: pluginResult callbackId: actionCallbackId];
}

-(void)locationManager:(CLLocationManager *)manager didStartMonitoringForRegion:(CLRegion *)region
{
    DEBUGLOG(@"didStartMonitoringForRegion");
    NSMutableDictionary* jResult = [[NSMutableDictionary alloc] init];

    [jResult setObject:@"startmonitoring" forKey:@"action"];
    [jResult setObject:getBeaconRegionInfo((CLBeaconRegion*)region) forKey:@"region"];

    CDVPluginResult* pluginResult = [CDVPluginResult resultWithStatus: CDVCommandStatus_OK messageAsDictionary:jResult];
    [pluginResult setKeepCallbackAsBool:TRUE];

    [self.commandDelegate sendPluginResult: pluginResult callbackId: actionCallbackId];
}

-(void)locationManager:(CLLocationManager *)manager didStopMonitoringForRegion:(CLRegion *)region
{
    DEBUGLOG(@"didStopMonitoringForRegion");
    NSMutableDictionary* jResult = [[NSMutableDictionary alloc] init];

    [jResult setObject:@"stopmonitoring" forKey:@"action"];
    [jResult setObject:getBeaconRegionInfo((CLBeaconRegion*)region) forKey:@"region"];

    CDVPluginResult* pluginResult = [CDVPluginResult resultWithStatus: CDVCommandStatus_OK messageAsDictionary:jResult];
    [pluginResult setKeepCallbackAsBool:TRUE];

    [self.commandDelegate sendPluginResult: pluginResult callbackId: actionCallbackId];
}

-(void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error
{
    DEBUGLOG(@"didFailWithError");
    NSMutableDictionary* jResult = [[NSMutableDictionary alloc] init];

    [jResult setObject:@"error" forKey:@"action"];
    [jResult setObject:[NSNumber numberWithLong:[error code]] forKey:@"code"];
    [jResult setObject:[error description] forKey:@"description"];

    CDVPluginResult* pluginResult = [CDVPluginResult resultWithStatus: CDVCommandStatus_OK messageAsDictionary:jResult];
    [pluginResult setKeepCallbackAsBool:TRUE];

    [self.commandDelegate sendPluginResult: pluginResult callbackId: actionCallbackId];
}
@end

@interface VirtualMgr_iBeacon : CDVPlugin {
}

@property (nonatomic, retain) NSMutableDictionary* clients;

- (void)pluginInitialize;
- (void)dispose;

@end


@implementation VirtualMgr_iBeacon

@synthesize clients;

-(void)pluginInitialize
{
    DEBUGLOG(@"pluginInitialize");
    [super pluginInitialize];

    self.clients = [[NSMutableDictionary alloc] init];
}

-(void)dispose
{
    DEBUGLOG(@"dispose");
    self.clients = nil;
    [super dispose];
}

-(void)getVersion:(CDVInvokedUrlCommand*) command
{
    NSMutableDictionary* result = [[NSMutableDictionary alloc] init];
    [result setObject:@"iOS" forKey:@"platform"];
    [result setObject:PLUGIN_VERSION forKey:@"version"];

    CDVPluginResult* pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsDictionary:result];
    [self.commandDelegate sendPluginResult: pluginResult callbackId: command.callbackId];
}

-(iBeaconScanClient*)getClientFromCommand:(CDVInvokedUrlCommand*) command
{
    // 1st parameter will be the clientId
    NSString* clientId = nil;
    if (command.arguments.count >= 1) {
        clientId = [command.arguments objectAtIndex:0];
    }

    // Find or create a new client ..
    iBeaconScanClient* client = [clients objectForKey:clientId];
    if (client == nil) {
        client = [[iBeaconScanClient alloc] initClientId: clientId withCommandDelegate: self.commandDelegate];
        [clients setObject:client forKey:clientId];
    }
    return client;
}

-(void)deleteClient:(CDVInvokedUrlCommand*) command
{
    CDVPluginResult* pluginResult = nil;
    NSString* clientId = nil;
    if (command.arguments.count >= 1) {
        clientId = [command.arguments objectAtIndex:0];
    }

    iBeaconScanClient* client = [clients objectForKey:clientId];
    if (client != nil) {
        [clients removeObjectForKey: clientId];
        [client dispose];
        pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK];
    } else {
        pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_ERROR messageAsString: @"Not found"];
    }

    [self.commandDelegate sendPluginResult: pluginResult callbackId: command.callbackId];
}

-(void)clientActionCallback:(CDVInvokedUrlCommand*) command
{
    iBeaconScanClient* client = [self getClientFromCommand:command];
    [client actionCallback:command];
}

-(void)clientRequestAlwaysAuthorisation:(CDVInvokedUrlCommand*) command
{
    iBeaconScanClient* client = [self getClientFromCommand:command];
    [client requestAlwaysAuthorisation:command];
}

-(void)clientRegisterRegion:(CDVInvokedUrlCommand*) command
{
    iBeaconScanClient* client = [self getClientFromCommand:command];
    [client registerRegion:command];
}

-(void)clientStartRangingBeaconsInRegion:(CDVInvokedUrlCommand*) command
{
    iBeaconScanClient* client = [self getClientFromCommand:command];
    [client startRangingBeaconsInRegion:command];
}

-(void)clientStopRangingBeaconsInRegion:(CDVInvokedUrlCommand*) command
{
    iBeaconScanClient* client = [self getClientFromCommand:command];
    [client stopRangingBeaconsInRegion:command];
}

-(void)clientStartMonitoringForRegion:(CDVInvokedUrlCommand*) command
{
    iBeaconScanClient* client = [self getClientFromCommand:command];
    [client startMonitoringForRegion:command];
}

-(void)clientStopMonitoringForRegion:(CDVInvokedUrlCommand*) command
{
    iBeaconScanClient* client = [self getClientFromCommand:command];
    [client stopMonitoringForRegion:command];
}

-(void)clientStopRangingForAllBeacons:(CDVInvokedUrlCommand*) command
{
    iBeaconScanClient* client = [self getClientFromCommand:command];
    [client stopRangingForAllBeacons:command];
}

-(void)clientRequestStateForRegion:(CDVInvokedUrlCommand*) command
{
    iBeaconScanClient* client = [self getClientFromCommand:command];
    [client requestStateForRegion:command];
}



@end
